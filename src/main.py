import os
import pickle
import numpy as np

import pandas as pd
import preparation as prep
from sklearn.model_selection import train_test_split

# TODO: Use seaborn as sns for data visualization

pd.set_option('max_columns', 500)

data = pd.read_csv('../data/raw/data_train.csv')
data_train, data_validate = train_test_split(data, test_size=0.3)

data_train = prep.process_data(data_train)
data_validate = prep.process_data(data_validate)

data_train_vectorized_map, data_validate_vectorized_map = prep.train_all_vectorizers(data_train, data_validate)
output = prep.train_all_with_settings(data_train, data_validate, data_train_vectorized_map,
                                      data_validate_vectorized_map)
best_outputs = {}
for genre in prep.genres_to_test:
    best_outputs[genre] = prep.get_best_output_for_genre(output, genre)
print(best_outputs)
prep.save_models(best_outputs)

f1_score = prep.calculate_f1_score_macro(best_outputs)
print(f1_score)

# Use serialized model:
data_test = pd.read_csv('../data/raw/data_test_all.csv')
data_test = prep.process_data(data_test)
data_test['Predictions'] = np.empty((len(data_test), 0)).tolist()
for genre in prep.genres_to_test:
    model_filename = [filename for filename in os.listdir('.') if filename.startswith(genre)][0]
    print(model_filename)
    vectorizer = model_filename.split(genre + '_')[1].split('_plot')[0]
    feature = 'plot' + model_filename.split('_plot')[1].split('_clf')[0]
    vectorized_feature = prep.vectorize_feature_with_vectorizer(data_test, feature, vectorizer)
    clf = pickle.load(open(model_filename, 'rb'))
    y_test = clf.predict(vectorized_feature)
    for i in range(0, len(data_test) - 1):
        if y_test[i] == 1:
            data_test['Predictions'][i].append(genre.split('is_')[1])

data_test = data_test[data_test.columns.drop(list(data_test.filter(regex='is_')))]
data_test = data_test[data_test.columns.drop(list(data_test.filter(regex='plot_synopsis_')))]
print(data_test.head(5))
data_test.to_csv(r'../data/processed/submission.csv', index=False, header=True)
