import pickle

from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
import numpy as np


def train_svc_model(list_of_classes, encoded_tags, vectorized_synopsises):
    preds = np.zeros(encoded_tags.shape)
    for i in range(0, encoded_tags.shape[1]):
        print(f'Model {i} stands for {list_of_classes[i]}')
        clf = SVC(class_weight='balanced')
        clf.fit(vectorized_synopsises, encoded_tags[:, i])
        filename = f'svc_model_{list_of_classes[i]}.sav'
        pickle.dump(clf, open(filename, 'wb'))
        preds[:, i] = (clf.predict_proba(vectorized_synopsises)[:, 1] >= 0.8).astype(int)
    return preds


def test_svc_model(list_of_classes, encoded_tags, vectorized_synopsises):
    preds = np.zeros(encoded_tags.shape)
    for i in range(0, encoded_tags.shape[1]):
        print(f'Model {i} stands for {list_of_classes[i]}')
        filename = f'svc_model_{list_of_classes[i]}.sav'
        clf = pickle.load(open(filename, 'rb'))
        preds[:, i] = (clf.predict_proba(vectorized_synopsises)[:, 1] >= 0.8).astype(int)
    return preds


def train_log_reg_model(list_of_classes, encoded_tags, vectorized_synopsises):
    preds = np.zeros(encoded_tags.shape)
    for i in range(0, encoded_tags.shape[1]):
        print(f'Model {i} stands for {list_of_classes[i]}')
        clf = LogisticRegression(class_weight='balanced')
        clf = clf.fit(vectorized_synopsises, encoded_tags[:, i])
        filename = f'log_reg_model_{list_of_classes[i]}.sav'
        pickle.dump(clf, open(filename, 'wb'))
        preds[:, i] = (clf.predict_proba(vectorized_synopsises)[:, 1] >= 0.8).astype(int)
    return preds


def test_log_reg_model(list_of_classes, encoded_tags, vectorized_synopsises):
    preds = np.zeros(encoded_tags.shape)
    for i in range(0, encoded_tags.shape[1]):
        print(f'Model {i} stands for {list_of_classes[i]}')
        filename = f'log_reg_model_{list_of_classes[i]}.sav'
        clf = pickle.load(open(filename, 'rb'))
        preds[:, i] = (clf.predict_proba(vectorized_synopsises)[:, 1] >= 0.8).astype(int)
    return preds


def train_log_reg_model_one_vs_rest(list_of_classes, encoded_tags, vectorized_synopsises):
    preds = np.zeros(encoded_tags.shape)
    for i in range(0, encoded_tags.shape[1]):
        print(f'Model {i} stands for {list_of_classes[i]}')
        lr = LogisticRegression()
        clf = OneVsRestClassifier(lr)
        clf.fit(vectorized_synopsises, encoded_tags[:, i])
        filename = f'log_reg_model_one_vs_rest_{list_of_classes[i]}.sav'
        pickle.dump(clf, open(filename, 'wb'))
        preds[:, i] = (clf.predict_proba(vectorized_synopsises)[:, 1] >= 0.8).astype(int)
    return preds


def test_log_reg_model_one_vs_rest(list_of_classes, encoded_tags, vectorized_synopsises):
    preds = np.zeros(encoded_tags.shape)
    for i in range(0, encoded_tags.shape[1]):
        print(f'Model {i} stands for {list_of_classes[i]}')
        filename = f'log_reg_model_one_vs_rest_{list_of_classes[i]}.sav'
        clf = pickle.load(open(filename, 'rb'))
        preds[:, i] = (clf.predict_proba(vectorized_synopsises)[:, 1] >= 0.8).astype(int)
    return preds


def train_naive_bayes(list_of_classes, encoded_tags, vectorized_synopsises):
    preds = np.zeros(encoded_tags.shape)
    for i in range(0, encoded_tags.shape[1]):
        print(f'Model {i} stands for {list_of_classes[i]}')
        clf = MultinomialNB()
        clf.fit(vectorized_synopsises, encoded_tags[:, i])
        filename = f'bayes_model_{list_of_classes[i]}.sav'
        pickle.dump(clf, open(filename, 'wb'))
        preds[:, i] = clf.predict(vectorized_synopsises)
    return preds


def test_naive_bayes(list_of_classes, encoded_tags, vectorized_synopsises):
    preds = np.zeros(encoded_tags.shape)
    for i in range(0, encoded_tags.shape[1]):
        print(f'Model {i} stands for {list_of_classes[i]}')
        filename = f'bayes_model_{list_of_classes[i]}.sav'
        clf = pickle.load(open(filename, 'rb'))
        preds[:, i] = clf.predict(vectorized_synopsises)
    return preds
