import pickle
import re

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.metrics import f1_score
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
import numpy as np
import matplotlib.pyplot as plt
from nltk import pos_tag
import nltk
import pandas as pd

SPLITTED_TAGS_COL_NAME = "splitted_tags"
SYNOPSIS_INITIAL_COL = "plot_synopsis"
CLEAN_SYNOPSIS_COL = "plot_synopsis_clean_col"
CLEAN_SYNOPSIS_NO_SW_COL = "plot_synopsis_clean_no_sw_col"
CLEAN_SYNOPSIS_NO_SW_ADJECTIVES_COL = "plot_synopsis_clean_no_sw_adj_col"
CLEAN_SYNOPSIS_NO_SW_NOUNS_COL = "plot_synopsis_clean_no_sw_nouns_col"
CLEAN_SYNOPSIS_NO_SW_NOUNS_ADJECTIVES_COL = "plot_synopsis_clean_no_sw_adj_nouns_col"
NOUN_TAGS = ['NN', 'NNS', 'NNP', 'NNPS', 'FW']
ADJECTIVE_TAGS = ['JJ', 'JJR', 'JJS', 'RB', 'RBR', 'RBS']


def process_data(data):
    new_data = pd.DataFrame()
    if 'tags' in data.columns:
        data[SPLITTED_TAGS_COL_NAME] = data.tags.str.split(', ')
        dummy_tags_df = pd.get_dummies(data[SPLITTED_TAGS_COL_NAME].apply(pd.Series).stack(), prefix='is').sum(level=0)
        new_data = dummy_tags_df
        new_data[SPLITTED_TAGS_COL_NAME] = data[SPLITTED_TAGS_COL_NAME]

    new_data['id'] = data.id
    new_data[SYNOPSIS_INITIAL_COL] = data[SYNOPSIS_INITIAL_COL]
    # print(new_data.head(5))
    new_data[CLEAN_SYNOPSIS_COL] = [clean_text(x) for x in new_data[SYNOPSIS_INITIAL_COL]]
    new_data[CLEAN_SYNOPSIS_NO_SW_COL] = [remove_stopwords(x) for x in new_data[CLEAN_SYNOPSIS_COL]]
    new_data[CLEAN_SYNOPSIS_NO_SW_ADJECTIVES_COL] = [select_only_pos(x, ADJECTIVE_TAGS) for x in
                                                     new_data[CLEAN_SYNOPSIS_NO_SW_COL]]
    new_data[CLEAN_SYNOPSIS_NO_SW_NOUNS_COL] = [select_only_pos(x, NOUN_TAGS) for x in
                                                new_data[CLEAN_SYNOPSIS_NO_SW_COL]]
    new_data[CLEAN_SYNOPSIS_NO_SW_NOUNS_ADJECTIVES_COL] = [select_only_pos(x, NOUN_TAGS + ADJECTIVE_TAGS) for x in
                                                           new_data[CLEAN_SYNOPSIS_NO_SW_COL]]

    return new_data


def vectorize_synopsises(data_train, data_validate, vectorizer, key):
    for feature in feature_cols:
        print(feature)
        vectorizer.fit(data_train[feature])
        data_train[feature + '_' + key] = vectorizer.transform(data_train[feature])
        data_validate[feature + '_' + key] = vectorizer.transform(data_validate[feature])

    return data_train, data_validate


def get_vectorizers_map():
    return {
        'cv': CountVectorizer(ngram_range=[1, 1], max_features=2000, binary=False),
        'cv_ngram_binary': CountVectorizer(ngram_range=[1, 2], max_features=2000, binary=True),
        'tfidf': TfidfVectorizer(max_features=2000, ngram_range=[1, 1]),
        'tfidf_ngram': TfidfVectorizer(max_features=2000, ngram_range=[1, 2])
    }


def get_clf_map():
    return {
        'clf_nb': MultinomialNB(),
        'clf_k_neighbours': KNeighborsClassifier(n_neighbors=5),
        'clf_random_forest': RandomForestClassifier(),
        'clf_grad_boost': GradientBoostingClassifier(),
        'clf_dec_tree': DecisionTreeClassifier()
    }


def train_all_vectorizers(data_train, data_validate):
    data_train_vectorized_map = {}
    data_validate_vectorized_map = {}
    for key in vectorizers_map:
        print(key)
        vectorizer = vectorizers_map[key]
        for feature in feature_cols:
            print(feature)
            vectorizer.fit(data_train[feature])
            data_train_vectorized_map[feature + '_' + key] = vectorizer.transform(data_train[feature])
            data_validate_vectorized_map[feature + '_' + key] = vectorizer.transform(data_validate[feature])
        vectorizers_map[key] = vectorizer
    return data_train_vectorized_map, data_validate_vectorized_map


def vectorize_feature_with_vectorizer(data_test, feature, vectorizer_key):
    vectorizer = vectorizers_map[vectorizer_key]
    return vectorizer.transform(data_test[feature])


def clean_text(text):
    # remove backslash-apostrophe
    text = re.sub("\'", "", text)
    # remove everything except alphabets
    text = re.sub("[^a-zA-Z]", " ", text)
    # remove whitespaces
    text = ' '.join(text.split())
    # convert text to lowercase
    text = text.lower()

    return text


# function to remove stopwords
def remove_stopwords(text):
    stop_words = set(nltk.corpus.stopwords.words('english'))
    no_stopword_text = [w for w in text.split() if not w in stop_words]
    return ' '.join(no_stopword_text)


def select_only_pos(text, list_of_tags):
    splitted_text = text.lower().split(', ')
    tags = pos_tag(splitted_text)
    res = []
    for w, pos in tags:
        if pos in list_of_tags:
            res.append(w)
    return ' '.join(res)


def save_models(best_outputs):
    for genre in genres_to_test:
        key = best_outputs[genre]['key']
        clf_key = 'clf'+key.split('clf')[1]
        # print(clf_key)
        filename = f'model/{key}.sav'
        pickle.dump(clf_map[clf_key], open(filename, 'wb'))


# get list of unique tags
def get_unique_tags_list(data):
    unique_tags = []
    for tag_list in data[SPLITTED_TAGS_COL_NAME]:
        for tag in tag_list:
            if tag not in unique_tags:
                unique_tags.append(tag)
    return unique_tags


def sort_tags_by_frequency(data):
    unique_tags = get_unique_tags_list(data)
    freqs = np.zeros(len(unique_tags))
    for i in range(0, len(unique_tags)):
        for row in data[SPLITTED_TAGS_COL_NAME]:
            if unique_tags[i] in row:
                freqs[i] += 1
    sorted_unique_tags = [x for _, x in sorted(zip(freqs, unique_tags), reverse=True)]
    sorted_freqs = sorted(freqs, reverse=True)

    # creating the bar plot
    # plt.figure(figsize=(10, 10))
    # plt.bar(sorted_unique_tags, sorted_freqs, color='maroon',
    #         width=0.4)
    # plt.title('Frequency of tags')
    # plt.xlabel('Tags')
    # plt.xticks(rotation=90)
    # plt.ylabel('Counts')
    # plt.show()
    return sorted_unique_tags


feature_cols = [
    SYNOPSIS_INITIAL_COL,
    CLEAN_SYNOPSIS_COL,
    CLEAN_SYNOPSIS_NO_SW_COL,
    CLEAN_SYNOPSIS_NO_SW_ADJECTIVES_COL,
    CLEAN_SYNOPSIS_NO_SW_NOUNS_COL,
    CLEAN_SYNOPSIS_NO_SW_NOUNS_ADJECTIVES_COL
]
clf_map = get_clf_map()
vectorizers_map = get_vectorizers_map()
# NOTE (Genres to distinguish are NOT the 5 of most popular. Most popular 5 are ['murder', 'violence', 'romantic', 'flashback', 'cult']
genres_to_test = ['is_murder', 'is_romantic', 'is_comedy', 'is_fantasy', 'is_flashback']


def train_all_with_settings(data_train, data_validate, data_train_vectorized_map, data_validate_vectorized_map):
    output = {}
    for clf_key in clf_map:
        for feature_key in feature_cols:
            for vectorizer_key in vectorizers_map:
                for genre in genres_to_test:
                    print(
                        'TRAIN: Genre: ' + genre +
                        ', Vectorizer: ' + vectorizer_key +
                        ', Feature: ' + feature_key +
                        ', CLF: ' + clf_key
                    )
                    clf = clf_map[clf_key]
                    clf.fit(data_train_vectorized_map[feature_key + '_' + vectorizer_key], data_train[genre].values)
                    clf_map[clf_key] = clf
                    y_train_pred = clf.predict(data_train_vectorized_map[feature_key + '_' + vectorizer_key])
                    y_validate_pred = clf.predict(data_validate_vectorized_map[feature_key + '_' + vectorizer_key])
                    output[genre + '_' + vectorizer_key + '_' + feature_key + '_' + clf_key] = {
                        'y_train': data_train[genre].values,
                        'y_train_pred': y_train_pred,
                        'y_validate': data_validate[genre].values,
                        'y_validate_pred': y_validate_pred,
                        'y_validate_f1_score': f1_score(data_validate[genre], y_validate_pred),
                        'key': genre + '_' + vectorizer_key + '_' + feature_key + '_' + clf_key
                    }
    return output


def get_best_output_for_genre(all_, genre):
    all_outputs_of_genre = {key: val for key, val in all_.items() if key.startswith(genre)}
    best_output = list(all_outputs_of_genre.values())[0]
    for _, output in all_outputs_of_genre.items():
        if output['y_validate_f1_score'] > best_output['y_validate_f1_score']:
            best_output = output
    return best_output


def calculate_f1_score_macro(best_outputs):
    return np.mean([output['y_validate_f1_score'] for _, output in best_outputs.items()])


def calculate_f1_score_micro(best_outputs):
    y_validate_true = np.concatenate([output['y_validate'] for _, output in best_outputs.items()])
    y_validate_pred = np.concatenate([output['y_validate_pred'] for _, output in best_outputs.items()])
    return f1_score(y_validate_true, y_validate_pred, average='micro')
